package rasmita.nurilda.app_x08_2c

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val FONT_SUBTITLE = "subFont"
    val FONT_DETAIL = "F"
    val DEF_FONT_SIZE = 20
    val DEF_TEXT = "Hello World"
    val bgHeader = "header"
    val bgMain = "background"
    val DEF_BACK = "White"
    val TITLE_FILM = "Jumanji"
    val DEFF_FILM = "Jumanji"
    val CONTENT_FILM = "Content Film"
    val DEFF_CONTENT_FILM = "Dalam Jumanji: The Next Level, keempat sahabat kembali lagi, tetapi permainannya telah berubah. Saat memutuskan masuk lagi ke Jumanji untuk menyelamatkan salah satu dari mereka, mereka menemukan segalanya tak seperti yang diduga. Para pemain harus melaju ke wilayah-wilayah tersembunyi dan belum dijamah, dari gurun nan gersang ke pegunungan bersalju, demi meloloskan diri dari permainan paling berbahaya di dunia."

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var menuInflater = menuInflater
        menuInflater.inflate(R.menu.optionmenu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting -> {
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun getBackground(myString: String, el:Boolean){
        if(myString.equals("Green")){
            if (el){
                constraintlayout.setBackgroundColor(Color.GREEN)
                txTitle.setTextColor(Color.BLACK)
                txSub.setTextColor(Color.BLACK)
                txDetail.setTextColor(Color.BLACK)
            }else{
                bgH.setBackgroundColor(Color.GREEN)
                txtHeader.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Blue")){
            if (el){
                constraintlayout.setBackgroundColor(Color.BLUE)
                txTitle.setTextColor(Color.WHITE)
                txSub.setTextColor(Color.WHITE)
                txDetail.setTextColor(Color.WHITE)
            }else{
                bgH.setBackgroundColor(Color.BLUE)
                txtHeader.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Yellow")){
            if (el){
                constraintlayout.setBackgroundColor(Color.YELLOW)
                txTitle.setTextColor(Color.BLACK)
                txSub.setTextColor(Color.BLACK)
                txDetail.setTextColor(Color.BLACK)
            }else{
                bgH.setBackgroundColor(Color.YELLOW)
                txtHeader.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Black")){
            if (el){
                constraintlayout.setBackgroundColor(Color.BLACK)
                txTitle.setTextColor(Color.WHITE)
                txSub.setTextColor(Color.WHITE)
                txDetail.setTextColor(Color.WHITE)
            }else{
                bgH.setBackgroundColor(Color.BLACK)
                txtHeader.setTextColor(Color.WHITE)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val wholeBack = preferences.getString(bgMain,DEF_BACK).toString()
        getBackground(wholeBack,true)
        val bgHd = preferences.getString(bgHeader,DEF_BACK).toString()
        getBackground(bgHd,false)
        txTitle.setText(preferences.getString(TITLE_FILM,DEFF_FILM))
        txSub.setTextSize(preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE).toFloat())
        txDetail.setText(preferences.getString(CONTENT_FILM,DEFF_CONTENT_FILM))
        txDetail.setTextSize(preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE).toFloat())
        Toast.makeText(this,"Perubahan telah disimpan.",Toast.LENGTH_SHORT).show()
    }
}