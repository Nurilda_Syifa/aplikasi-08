package rasmita.nurilda.app_x08_2c

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener{
    var background: String = ""
    var selectedBack : String = ""
    var currentBack = ""
    var checkHD = ""
    var getTitle = ""

    lateinit var adapterSpin : ArrayAdapter<String>
    val arrayBackground = arrayOf("Blue","Yellow","Green","Black")

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    //    spinner
    val bgMain = "background"
    val DEF_BACK = "White"
    val bgHeader = "header"
    var checkedRadio = ""
    val TITLE_FILM = "Jumanji"
    val CONTENT_FILM = "Content Film"
    val DEFF_CONTENT_FILM = "Dalam Jumanji: The Next Level, keempat sahabat kembali lagi, tetapi permainannya telah berubah. Saat memutuskan masuk lagi ke Jumanji untuk menyelamatkan salah satu dari mereka, mereka menemukan segalanya tak seperti yang diduga. Para pemain harus melaju ke wilayah-wilayah tersembunyi dan belum dijamah, dari gurun nan gersang ke pegunungan bersalju, demi meloloskan diri dari permainan paling berbahaya di dunia."
    val DEFF_FILM = "Jumanji"
    val FONT_SUBTITLE = "subFont"
    val DEF_FONT_SIZE = 20
    val FONT_DETAIL = "F"

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(bgMain,selectedBack)
        prefEditor.putString(bgHeader,checkedRadio)
        prefEditor.putString(TITLE_FILM,edJudul.text.toString())
        prefEditor.putInt(FONT_SUBTITLE,skHeader.progress)
        prefEditor.putString(CONTENT_FILM,edDetail.text.toString())
        prefEditor.putInt(FONT_DETAIL,skTitle.progress)
        prefEditor.commit()
        System.exit(0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        currentBack = preferences.getString(bgMain,DEF_BACK).toString()
        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayBackground)
        checkHD = preferences.getString(bgHeader,DEF_BACK).toString()
        edJudul.setText(preferences.getString(TITLE_FILM,DEFF_FILM))
        edDetail.setText(preferences.getString(CONTENT_FILM,DEFF_CONTENT_FILM))
        skHeader.progress = preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE)
        skTitle.progress = preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE)
        spBackground.adapter = adapterSpin
        spBackground.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if(background.equals("Blue")){
                    selectedBack = "Blue"
                }
                if(background.equals("Yellow")){
                    selectedBack = "Yellow"
                }
                if(background.equals("Green")){
                    selectedBack = "Green"
                }
                if(background.equals("Black")){
                    selectedBack = "Black"
                }
            }
        }

        radioGroup.setOnCheckedChangeListener{group, checkedId ->
            when(checkedId){
                R.id.rdBlue->{
                    checkedRadio = "Blue"
                }R.id.rdYellow->{
                checkedRadio = "Yellow"
            }R.id.rdGreen->{
                checkedRadio = "Green"
            }R.id.rdBlack->{
                checkedRadio = "Black"
            }
            }
        }

        btnSimpan.setOnClickListener(this)
    }



    override fun onStart() {
        super.onStart()
        spBackground.setSelection(getIndex(spBackground,currentBack))
        getChecked()
    }

    fun getIndex(spinner: Spinner, myString: String) : Int {
        var a = spinner.count
        var b : String =""
        for(i in 0 until a){
            b = arrayBackground.get(i)
            if(b.equals(myString,ignoreCase = true)){
                return  i
            }
        }
        return 0
    }

    fun getChecked(){
        if(checkHD.equals("Blue")){
            rdBlue.isChecked = true
        }
        if(checkHD.equals("Green")){
            rdGreen.isChecked = true
        }
        if(checkHD.equals("Yellow")){
            rdYellow.isChecked = true
        }
        if(checkHD.equals("Black")){
            rdBlack.isChecked = true
        }
    }
}